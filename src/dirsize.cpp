#include <string>
#include <iostream>
#include <vector>
#include <filesystem>

const int MAX_DEPTH = 2;

class Directory {
  public:
    Directory(std::filesystem::path);

    std::filesystem::directory_iterator& get_iterator();
    const std::filesystem::path& get_path();
    void add(long additional);
    long get_size();

  private:
    std::filesystem::path path;
    long size;
    std::filesystem::directory_iterator iterator;
};

std::filesystem::path setup_path(int argc, char** argv) {
  if (argc > 1)
    return std::filesystem::path(argv[1]);
  return std::filesystem::path(".");
}

std::filesystem::directory_iterator setup_iterator(int argc, char** argv) {
  std::filesystem::path path;
  if (argc > 1)
    path = std::filesystem::path(argv[1]);
  else
    path = std::filesystem::path(".");

  return std::filesystem::directory_iterator(path);
}

void dump_data(const std::filesystem::path p, long size, int depth) {
  if (depth < MAX_DEPTH)
    std::cout << size << "\t" << p << std::endl;
}

int main(int argc, char** argv) {
  std::vector<Directory> paths { {setup_path(argc, argv)} };

  while (paths.size() > 0){
    Directory dir = paths.back();
    paths.pop_back();
    auto& i = dir.get_iterator();
    bool done = true;

    for (auto const& en : i) {
        if (en.is_directory()) {
          std::filesystem::path pa(en);

          ++i; // advance so that we won't jump into the same child directory again

          paths.push_back(dir); // parent directory in progress
          paths.push_back(Directory(pa)); // directory we are now entering
          done = false;
          break;
        } else {
          dump_data(en, en.file_size(), paths.size());
          dir.add(en.file_size());
        }
    }

    if (done) {
      // we dump information for the directory we just traversed here
      dump_data(dir.get_path(), dir.get_size(), paths.size());
      if (paths.size() > 0) {
        paths.back().add(dir.get_size());
      }
    }
  }

  return 0;
}

Directory::Directory(std::filesystem::path path) {
  this->path = path;
  iterator = std::filesystem::directory_iterator(path);
  size = 0;
}

std::filesystem::directory_iterator& Directory::get_iterator() {
  return iterator;
}

const std::filesystem::path& Directory::get_path() {
  return path;
}

void Directory::add(long additional) {
  size += additional;
}

long Directory::get_size() {
  return size;
}
